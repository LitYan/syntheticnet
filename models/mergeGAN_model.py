import torch
from torch import nn
import numpy as np
from .base_model import BaseModel
from . import networks
class MergeGANModel(BaseModel):

    def name(self):
        return 'MergeGANModel'
    def initialize(self, opt):
        BaseModel.initialize(self,opt)
        encoder_input_nc=self.label_nc+self.boundary_nc if not opt.no_instance else self.label_nc
        n_downsample,initial_conv_dim=self.init_conv_dim()

        # basic lambda params for updating loss_D_A
        self.gamma_k=opt.gamma_k
        self.lambda_k=opt.lambda_k
        self.k=opt.k

        # specify the models you want to save to the disk. The program will call base_model.save_networks and base_model.load_networks
        self.model_names=['G_A']
        self.visual_names=['real_label','real_edge','fake_label','fake_edge'] if not opt.no_instance else ['real_label','fake_label']



        #####define networks
        # z Encoder
        if self.use_z_encoder:
            self.netE_A = networks.define_AE(input_nc=encoder_input_nc, output_nc=self.z_num, z_num=self.z_num,
                                             initial_conv_dim=initial_conv_dim, net='encoder',
                                             n_downsample=n_downsample, nf=self.ngf, norm='batch',
                                             vae_like=True, gpu_ids=self.gpu_ids)
            self.model_names.append('E_A')

        # Generator network
        self.netG_A=networks.define_AE(input_nc=self.z_num,output_nc=self.label_nc,
                                       initial_conv_dim=initial_conv_dim,net='decoder',
                                       n_downsample=n_downsample,nf=self.ngf,nl='elu',
                                       gpu_ids=self.gpu_ids)
        if self.use_label_encoder:
            self.netL_A=networks.define_AE(input_nc=self.boundary_nc,output_nc=self.label_nc,
                                           net='label_encoder',nf=self.ngf,gpu_ids=self.gpu_ids)
            self.model_names.append('L_A')

        if self.isTrain:
            # Discriminator
            self.netD_A = networks.define_AE(input_nc=encoder_input_nc, output_nc=encoder_input_nc, z_num=self.z_num,
                                             initial_conv_dim=initial_conv_dim, net='auto_encoder',
                                             n_downsample=n_downsample, nf=self.ndf, nl='elu',
                                             gpu_ids=self.gpu_ids)
            self.model_names.append('D_A')

            # if self.use_net_D_A_2:
            #     pass

            #define loss functions
            self.criterionL1=nn.L1Loss()
            # self.criterionGAN=networks.GANLoss(use_lsgan=not opt.no_lsgan,tensor=self.Tensor)

            # initialize optimizers
            self.optimizers = []
            #optimizer G
            params = list(self.netG_A.parameters())
            if self.use_label_encoder:
                params+=list(self.netL_A.parameters())
            self.optimizer_G=torch.optim.Adam(params,lr=opt.lr,betas=(opt.beta1,0.999))
            self.optimizers.append(self.optimizer_G)
            #optimizer D
            self.optimizer_D=torch.optim.Adam(self.netD_A.parameters(),lr=opt.lr,betas=(opt.beta1,0.999))
            self.optimizers.append(self.optimizer_D)
            #optimizer E
            if self.use_z_encoder:
                self.optimizer_E=torch.optim.Adam(self.netE_A.parameters(),lr=opt.lr,betas=(opt.beta1,0.999))
                self.optimizers.append(self.optimizer_E)

    def get_z_random(self, batch_size, nz, random_type='gauss'):
        if random_type == 'uni':
            z = torch.rand(batch_size, nz) * 2.0 - 1.0
        elif random_type == 'gauss':
            z = torch.randn(batch_size, nz)
        return z.to(self.device)

    def get_z_encode(self, input_image):
        mu, logvar = self.netE_A.forward(input_image) #logvar=log(sigma^2) mu=mu
        std = logvar.mul(0.5).exp_()
        eps = self.get_z_random(std.size(0), std.size(1))
        z = eps.mul(std).add_(mu)
        return z, mu, logvar

    def encode_input(self,label_map,inst_map=None):
        # print(label_map.type())
        if self.label_nc == 1:
            self.real_label=label_map.to(self.device)
        else:
            # create one-hot vector for label map
            # print(np.unique(label_map.numpy()))
            size = label_map.size()
            # print(size)
            oneHot_size = (size[0], self.opt.label_nc, size[2], size[3])
            # print(oneHot_size)
            self.real_label = self.Tensor(torch.Size(oneHot_size)).zero_()
            # print(self.real_label.type())
            self.real_label = self.real_label.scatter_(1, label_map.data.long().to(self.device), 1.0)

        # get edges from instance map
        if not self.opt.no_instance:
            inst_map = inst_map.data.to(self.device)
            # print(inst_map.type())
            self.real_edge = self.get_edges(inst_map)
            input_label = torch.cat((self.real_label, self.real_edge), dim=1)
        else:
            input_label=self.real_label.detach()
        return input_label

    def get_edges(self, t):
        Tensor = torch.cuda.ByteTensor if self.gpu_ids else torch.ByteTensor
        edge = Tensor(t.size()).zero_()
        edge[:,:,:,1:] = edge[:,:,:,1:] | (t[:,:,:,1:] != t[:,:,:,:-1])
        edge[:,:,:,:-1] = edge[:,:,:,:-1] | (t[:,:,:,1:] != t[:,:,:,:-1])
        edge[:,:,1:,:] = edge[:,:,1:,:] | (t[:,:,1:,:] != t[:,:,:-1,:])
        edge[:,:,:-1,:] = edge[:,:,:-1,:] | (t[:,:,1:,:] != t[:,:,:-1,:])

        # create one-hot vector for edge map
        size = edge.size()
        oneHot_size = (size[0], 2, size[2], size[3])
        edge_oneHot = self.Tensor(torch.Size(oneHot_size)).zero_()
        edge_oneHot = edge_oneHot.scatter_(1, edge.data.long().to(self.device), 1.0)

        return edge_oneHot.float()
    def set_input(self, input):
        inst=input['inst'].to(self.device)
        # self.image=input['image'].to(self.device)
        label=input['label']
        if label is not None:
            self.input_label = self.encode_input(label, inst)

        else:
            pass
    def forward(self):
        # #encode inputs
        # self.input_label=self.encode_input(label,inst)
        if self.use_z_encoder:
            #get encoded z
            z,self.mu,self.logvar=self.get_z_encode(self.input_label)
        else:
            #get random z
            z=self.get_z_random(self.opt.batchSize,self.z_num)

        #fake generation
        self.fake_edge,self.fake_label=self.netG_A(z)
        if self.use_label_encoder:
            self.fake_label=self.netL_A(self.fake_edge)

        self.output_label = self.fake_label

        if not self.opt.no_instance:
            self.output_label=torch.cat((self.fake_label,self.fake_edge),dim=1)



    def backward_D(self,real,fake):
        # D_fake
        pred_fake = self.netD_A(fake)
        self.loss_D_fake = self.criterionL1(pred_fake, fake)
        # D_real
        pred_real = self.netD_A(real)
        self.loss_D_real = self.criterionL1(pred_real, real)

        # loss D_A
        self.loss_D_A = self.loss_D_real - self.k * self.loss_D_fake
        self.loss_D_A.backward()

        # operation for updating k
        temp_k = self.k + self.lambda_k * (self.gamma_k * self.loss_D_real - self.loss_D_fake)
        temp_k = temp_k.item()#tensor的值
        self.k = min(max(temp_k, 0), 1)

        self.loss_names+=['D_real','D_fake','D_A']



    def backward_G(self,fake):
        # loss G_A
        pred_fake = self.netD_A(fake)
        self.loss_G_A = self.criterionL1(pred_fake, fake)
        self.loss_G_A.backward()
        self.loss_names.append('G_A')

    def backward_EG(self,real,fake):
        # loss GAN
        pred_fake = self.netD_A(fake)
        self.loss_G_A = self.criterionL1(pred_fake, fake)
        # KLloss
        if self.use_z_encoder and self.opt.lambda_k1>0.0:
            # see Appendix B from VAE paper:
            # Kingma and Welling. Auto-Encoding Variational Bayes. ICLR, 2014
            # https://arxiv.org/abs/1312.6114
            # 0.5 * sum(1 + log(sigma^2) - mu^2 - sigma^2)
            self.loss_kl=torch.sum(1+self.logvar-self.mu.pow(2)-self.logvar.exp())*(-0.5*self.opt.lambda_k1)
        else:
            self.loss_kl=0.0
        # reconstruction L1
        if self.use_z_encoder and self.opt.lambda_L1 > 0.0:
            self.loss_G_A_L1=self.criterionL1(fake,real)*self.opt.lambda_L1
        else:
            self.loss_G_A_L1=0.0
        self.loss_EG_A=self.loss_G_A+self.loss_kl+self.loss_G_A_L1
        self.loss_EG_A.backward()
        self.loss_names += ['G_A', 'kl','G_A_L1','EG_A']


    def update_D(self):
        self.set_requires_grad(self.netD_A,True)
        #update D_A
        self.optimizer_D.zero_grad()
        self.backward_D(self.input_label,self.output_label.detach())
        self.optimizer_D.step()

    def update_G_E(self):
        # update G E
        self.set_requires_grad(self.netD_A,False)
        self.optimizer_G.zero_grad()
        self.optimizer_E.zero_grad()
        self.backward_EG(self.input_label,self.output_label)
        self.optimizer_G.step()
        self.optimizer_E.step()
    def update_G(self):
        #update G alone
        self.set_requires_grad(self.netD_A,False)
        self.optimizer_G.zero_grad()
        self.backward_G(self.output_label)
        self.optimizer_G.step()

    def optimize_parameters(self):
        self.forward()
        self.update_D()
        if self.use_z_encoder:
            self.update_G_E()
        else:
            self.update_G()


