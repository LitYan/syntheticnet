from options import TrainOptions
from data.data_loader import CreateDataLoader
from models.mergeGAN_model import MergeGANModel
from util.visualizer import Visualizer
import os
import numpy as np
import time
if __name__ == '__main__':
    opt=TrainOptions().parse()
    iter_path=os.path.join(opt.checkpoints_dir,opt.name,'iter.txt')
    if opt.continue_train:
        try:
            start_epoch,epoch_iter=np.loadtxt(iter_path,delimiter=',',dtype=int)
        except:
            start_epoch,epoch_iter=1,0
        print('Resuming from epoch %d at itertion %d'%(start_epoch,epoch_iter))
    else:
        start_epoch, epoch_iter = 1, 0

    if opt.debug:
        opt.display_freq = 1
        opt.print_freq = 1
        opt.niter = 1
        opt.save_epoch_freq=1
        opt.save_latest_freq=5
        opt.max_dataset_size = 10
    data_loader = CreateDataLoader(opt)
    dataset = data_loader.load_data()
    dataset_size = len(data_loader)
    print('#training images = %d' % dataset_size)

    total_steps=(start_epoch-1)*dataset_size+epoch_iter

    display_delta=total_steps % opt.display_freq
    print_delta=total_steps % opt.print_freq
    save_delta=total_steps % opt.save_latest_freq

#     print(display_delta,print_delta,save_delta)

    model = MergeGANModel()
    model.setup(opt)
    visualizer = Visualizer(opt)

    for epoch in range(start_epoch,opt.niter+1):
        epoch_start_time=time.time()
        if epoch !=start_epoch:
            epoch_iter=epoch_iter % dataset_size
        for i,data in enumerate(dataset):
            if total_steps % opt.print_freq ==print_delta:
                iter_start_time=time.time()
            total_steps+=opt.batchSize
            epoch_iter+=opt.batchSize

            # whether to collect output images
            save_fake=total_steps % opt.display_freq == display_delta

            ##############Forward##################
            model.set_input(data)
            model.optimize_parameters()
            ##############Display results and errors ##############
            loss_dict=model.get_current_losses()
            if total_steps % opt.print_freq ==print_delta:
#                 print('plot loss')
                errors={k:v if not isinstance(v,int) else v for k,v in loss_dict.items()}
                t=(time.time()-iter_start_time)/opt.print_freq
                visualizer.print_current_errors(epoch,epoch_iter,errors,t)
                visualizer.plot_current_errors(errors,total_steps)

            ### display output images
            if save_fake:
#                 print('plot results')
                visuals=model.get_current_visuals()
                visualizer.display_current_results(visuals,epoch,total_steps)
            ### save latest model
            if total_steps % opt.save_latest_freq ==save_delta:
                print('saving the latest model (epoch %d,total_steps %d'%(epoch,total_steps))
                save_suffix= 'iter_%d'%total_steps if opt.save_by_iter else 'latest'
                model.save_networks(save_suffix)
                np.savetxt(iter_path,(epoch,epoch_iter),delimiter=',',fmt='%d')

            if epoch_iter >= dataset_size:
                break

        ### save model for this epoch
        if epoch % opt.save_epoch_freq==0:
            print('saving the model at the end of epoch %d,total_steps %d' % (epoch, total_steps))
            model.save_networks('latest')
            model.save_networks(epoch)
            np.savetxt(iter_path,(epoch+1,0),delimiter=',',fmt='%d')

        ### end of epoch
        iter_end_time = time.time()
        print('End of epoch %d / %d \t costing %d sec' % (epoch, opt.niter, time.time() - epoch_start_time))
        model.update_learning_rate()



