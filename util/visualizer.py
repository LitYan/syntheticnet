import numpy as np
import os
import ntpath
import time
from . import util
import scipy.misc
from tensorboardX import SummaryWriter
try:
    from StringIO import StringIO  # Python 2.7
except ImportError:
    from io import BytesIO         # Python 3.x

class Visualizer():
    def __init__(self, opt):
        # self.opt = opt
        self.tf_log = opt.tf_log
        self.win_size = opt.display_winsize
        self.name = opt.name
        if self.tf_log:
#             import tensorflow as tf
#             self.tf = tf
            self.log_dir = os.path.join(opt.checkpoints_dir, opt.name, 'logs')
            self.writer = SummaryWriter(self.log_dir)#tf.summary.FileWriter(self.log_dir)

        self.log_name = os.path.join(opt.checkpoints_dir, opt.name, 'loss_log.txt')
        with open(self.log_name, "a") as log_file:
            now = time.strftime("%c")
            log_file.write('================ Training Loss (%s) ================\n' % now)

    # |visuals|: dictionary of images to display or save
    def display_current_results(self, visuals, epoch, step):
        if self.tf_log: # show images in tensorboard output
            #add_images(tag, img_tensor, global_step=None, walltime=None, dataformats='NCHW')
#             img_summaries = []
            for label, image_numpy in visuals.items():
#                 print(image_numpy.shape)
                # Write the image to a string
                self.writer.add_image(label,image_numpy,step,dataformats='HWC')
#                 try:
#                     s = StringIO()
#                 except:
#                     s = BytesIO()
#                 scipy.misc.toimage(image_numpy).save(s, format="jpeg")
#                 # Create an Image object
#                 img_sum = self.tf.Summary.Image(encoded_image_string=s.getvalue(), height=image_numpy.shape[0], width=image_numpy.shape[1])
#                 # Create a Summary value
#                 img_summaries.append(self.tf.Summary.Value(tag=label, image=img_sum))

#             # Create and write Summary
#             summary = self.tf.Summary(value=img_summaries)
#             self.writer.add_summary(summary, step)



    # errors: dictionary of error labels and values
    def plot_current_errors(self, errors, step):
        if self.tf_log:
#             self.writer.add_scalars('loss',errors,step)
            for tag, value in errors.items():
                self.writer.add_scalar(tag, value, step, walltime=None)
#                 summary = self.tf.Summary(value=[self.tf.Summary.Value(tag=tag, simple_value=value)])
#                 self.writer.add_summary(summary, step)

    # errors: same format as |errors| of plotCurrentErrors
    def print_current_errors(self, epoch, i, errors, t):
        message = '(epoch: %d, iters: %d, time: %.3f) ' % (epoch, i, t)
        for k, v in errors.items():
            if v != 0:
                message += '%s: %.3f ' % (k, v)

        print(message)
        with open(self.log_name, "a") as log_file:
            log_file.write('%s\n' % message)

    # save image to the disk
    def save_images(self, webpage, visuals, image_path):
        image_dir = webpage.get_image_dir()
        short_path = ntpath.basename(image_path[0])
        name = os.path.splitext(short_path)[0]

        webpage.add_header(name)
        ims = []
        txts = []
        links = []

        for label, image_numpy in visuals.items():
            image_name = '%s_%s.jpg' % (name, label)
            save_path = os.path.join(image_dir, image_name)
            util.save_image(image_numpy, save_path)

            ims.append(image_name)
            txts.append(label)
            links.append(image_name)
        webpage.add_images(ims, txts, links, width=self.win_size)