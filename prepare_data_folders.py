import shutil
import os
root_path='./datasets/cityscapes/'
in_path=root_path+'gtFine/val/'
out_label_path=root_path+'test_label/'
out_inst_path=root_path+'test_inst/'
os.makedirs(out_label_path)
os.makedirs(out_inst_path)
for r,p,f in os.walk(in_path):
    for name in f:
        if 'instanceIds' in name:
            shutil.copy(os.path.join(r,name),out_inst_path+name)
        elif 'labelIds' in name:
            shutil.copy(os.path.join(r,name),out_label_path+name)

